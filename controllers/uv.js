const express = require('express')
  , router = express.Router()
  , UV = require('../models/uv');

router.post('/', (req, res, next) => {
  UV.create(req, res, next);
});

router.get('/', (req, res) => {
  UV.getCurrent(function (err, weather, uv) {
    res.render('index', {weather: weather, uv: uv})
  });
});

module.exports = router;
