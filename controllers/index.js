const express = require('express');
const router = express.Router();

router.use('/uv', require('./uv'));

router.get('/', (req, res) => {
  res.render('index')
});

module.exports = router;
