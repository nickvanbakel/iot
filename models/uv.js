var mysql = require('mysql');

var con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  port: process.env.DB_PORT
});

// Create new uv in your database and return its id
exports.create = function(req, res, next) {
  //Current date
  const date = new Date().toISOString().slice(0, 19).replace('T', ' ');

  var sql = "INSERT INTO uv_data (datetime, value) VALUES ('"+ date +"', "+ req.body.value +");";

  con.query(sql, function (err, result) {
    if (err) {
      res.sendStatus(400).json({
        "error": err
      });
    }

    res.status(200).json({
      "message": "Record created",
      "object": result
    });
  });
};

// Get all comments
exports.getCurrent = function(cb) {
  var sql = "SELECT value FROM uv_data ORDER BY datetime DESC limit 1";

  var request = require('request');

  const options = {
    host: 'https://api.darksky.net',
    port: 443,
    path: '/forecast/' + process.env.DARKSKY_API_KEY + '/52.306084,-4.690704?lang=nl&units=si',
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const requestPromise = new Promise(((resolve, reject) => {
    request.get('https://api.darksky.net/forecast/' + process.env.DARKSKY_API_KEY + '/52.306084,-4.690704?lang=nl&units=si',options,function(err,res,body){
      if(err) cb(err, null, null)
      if(res.statusCode === 200 ) {
        resolve(JSON.parse(body).currently);
      }
    });
  }));

  const databasePromise = new Promise(((resolve, reject) => {
    con.query(sql, function (err, result) {
      resolve({value: result[0].value});
    });
  }));

  Promise.all([requestPromise, databasePromise])
      .then(values => cb(null, values[0], values[1]))
      .catch(error => console.log(error))
};
